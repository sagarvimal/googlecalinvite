'''
Sample file to create an invite and send to multiple people as
invitees
'''

from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

import datetime


try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Calendar API Python Quickstart'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else: # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials

'''
The whole request json object. All parameters to be set
inside this object.
For further list of attributes to insert in this, please
refer the Request body of this section:
https://developers.google.com/google-apps/calendar/v3/reference/events/insert
'''
event = {
  'summary': 'Zynga Test invite',
  'location': '3rd Floor POD a',
  'description': 'Okay this is a junk invite, but this is fun, trust me!',
  'start': {
    'dateTime': '2017-04-11T11:55:00+05:30',
    'timeZone': 'Asia/Kolkata',
  },
  'end': {
    'dateTime': '2017-04-11T12:00:00+05:30',
    'timeZone': 'Asia/Kolkata',
  },
  'recurrence': [
    'RRULE:FREQ=DAILY;COUNT=2'
  ],
  'attendees': [
    {'email': 'deepthi.gr11@gmail.com'},
    {'email': 'vimalsagartiwari@gmail.com'},
    {'email': 'arushi.parashari@gmail.com'}
  ]
}

'''
TODO: Add a module to load all the mail ids in the attendees section of the above object
'''
#def addMoreAttendees():



'''
Module to create an event.
'''
def create_event(event):
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('calendar', 'v3', http=http)
    event = service.events().insert(calendarId='primary', body=event, sendNotifications=True).execute()
    print ('Event created: %s' , (event.get('htmlLink')))
    print('summary', event.get('summary'))
    return event.get('htmlLink')

'''
main call
'''
create_event(event)
