This requires an app secret json, which has authenticated keys and urls when the api has been turned on for an email id. Also requires google-api python client.
To install google-apps calendar api follow this: https://developers.google.com/google-apps/calendar/quickstart/python
To troubleshoot issues with 'six' module, follow the troubleshooting steps mentioned at the bottom of the article.

Incase of doubts ping me on jaiwardhan.live@gmail.com
